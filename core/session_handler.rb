class SessionHandler

  attr_accessor :sin, :headers, :raw_session_cookie 

  def login
    puts 'No username or password provided' and return if !@username || !@password
    logout if @sin or @headers

    response = post(LOGIN_URL, login_params, false)
    @headers = generate_headers(response)
    @raw_session_cookie = parse_cookies(response["set-cookie"])

    set_sin
    if DEBUG && @sin && @headers
      puts 'Logged in.'
    elsif DEBUG
      puts 'ERROR login'
    end
  end

  def get(url, with_sin=true)
    uri = URI(url)
    if @sin && with_sin
      if url[-3..-1] == 'php'
        uri = URI(url + '?' + @sin) 
      else
        uri = URI(url + '&' + @sin) 
      end
    end
    query = Net::HTTP::Get.new(uri)

    if @headers
      @headers.each do |k, v|
        query.add_field(k.to_s, v)
      end
    end

    puts "GET: #{uri}" if DEBUG
    response = Net::HTTP.start(uri.hostname, uri.port) {|http|
      http.request(query)
    }
    if response.body[/Sesja niewazna/]
      login
      response = get(url, with_sin)
    end
    return response
  end

  def post(url, data, with_sin=true)
    uri = URI(url)
    if @sin && with_sin
      if url[-3..-1] == 'php'
        uri = URI(url + '?' + @sin) 
      else
        uri = URI(url + '&' + @sin) 
      end
    end
    query = Net::HTTP::Post.new(uri)
    query.set_form_data(data)

    if @headers
      @headers.each do |k, v|
        query.add_field(k.to_s, v)
      end
    end

    puts "POST: #{uri}" if DEBUG
    response = Net::HTTP.start(uri.hostname, uri.port) {|http|
      http.request(query)
    }
    if response.body[/Sesja niewazna/]
      puts 'ssss'
      login
      response = post(url, data, with_sin)
    end
    return response
  end

  def logout
    res = get(@universe_address + "/logout.php", false)
    if res.body[/Zostales pomyslnie wylogowany./]
      clear_variables
      puts 'Zostales pomyslnie wylogowany.' if DEBUG
      return true
    end
    false
  end

  private
  def parse_cookies(cookie)
    parsed = cookie[/(PHPSESSID[^;]*;)/]
    parsed += ' ' + cookie[/(OGamu9[^;]*;)/]
    parsed += ' ' + cookie[/(utfs[^;]*;)/]
  end

  def generate_headers(response)
    headers = HEADERS
    headers['Cookie'] = parse_cookies(response["set-cookie"])
    headers
  end

  def set_sin
    res = get(@universe_address + "/leftmenu.php", false)
    @sin = res.body[/sin=[^"]*"/][0..-2]
    puts "New sin: #{@sin}" if DEBUG
  end

  def clear_variables
    @sin = nil
    @headers = nil
    @raw_session_cookie = nil
  end

  def login_params
    params = LOGIN_PARAMS
    params['username'] = @username
    params['password'] = @password
    params
  end
end
# if not defined user const
UNIVERSE_ADDRESS = 'http://uni10.ogam.net.pl' unless defined? UNIVERSE_ADDRESS
FAST_SPY_PROBE_COUNT = 5000 unless defined? FAST_SPY_PROBE_COUNT
RESOURCE_CAP_TO_PLUNDER = 3000000000 unless defined? RESOURCE_CAP_TO_PLUNDER

# global const
DEBUG = false
LOGIN_URL = UNIVERSE_ADDRESS + "/login.php"
LOGIN_PARAMS = {
    Uni: LOGIN_URL,
    username: '',
    password: '',
    timestamp: '1173621187',
    v: '2',
    submit: 'Zaloguj się'
  }
HEADERS = {
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'Accept-Encoding': 'gzip, deflate',
  'Accept-Language': 'pl-PL,pl;q=0.9,en-US;q=0.8,en;q=0.7',
  'Connection': 'keep-alive',
  'Origin': 'http://uni10.ogam.net.pl',
  'Upgrade-Insecure-Requests': '1'
}
FLEET_CAPACITY = {  
  ship217: 150000,
  ship203: 25000,
  ship202: 500
}
FLEET_PARAMS = {
  "ship202"=>"0", # maly transporter
  "ship203"=>"0", # duzy transporter
  "ship204"=>"0", # lekki mysliwiec
  "ship205"=>"0", # ciezki mysliwiec
  "ship206"=>"0", # krazownik
  "ship207"=>"0", # OW
  "ship208"=>"0", # koloniz
  "ship209"=>"0", # recykler
  "ship210"=>"0", # sonda
  "ship211"=>"0", # bombowiec
  "ship212"=>"0", # satelita
  "ship213"=>"0", # niszczyciel
  "ship214"=>"0", # gwiazda smierci
  "ship215"=>"0", # pancernik
  "ship216"=>"0", # fleet slow
  "ship217"=>"0", # ultra transporter
  "epedition"=>"0"# ?
}
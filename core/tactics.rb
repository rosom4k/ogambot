class Bot < SessionHandler

  attr_accessor :queue

  def goua_tactics(mplanet, kplanet, dplanet, main_building_cap=50, side_buildings_cap=20)    
    # first need to fill side cap
    casem = build_tactics(mplanet, ['1', '2', '3'], side_buildings_cap)
    casek = build_tactics(kplanet, ['1', '2', '3'], side_buildings_cap)
    cased = build_tactics(dplanet, ['1', '2', '3'], side_buildings_cap)
    
    # then if side rule is completed
    build_tactics(mplanet, ['1'], main_building_cap) if !casem
    build_tactics(kplanet, ['2'], main_building_cap) if !casek
    build_tactics(dplanet, ['3'], main_building_cap) if !cased
  end

  def build_tactics(planet, building_ids, building_level_cap)
    switch_planet(planet, false)
    building = buildings.select{|b| building_ids.map{|q| q.to_s}.include?(b[:id])}.sort{|a, b| a[:level] <=> b[:level]}.first

    ignore_index = 0
    if building[:id] == '31' && is_researching?
      ignore_index += 1
      puts "Ignoring Lab because of researching"
      building = buildings.select{|b| building_ids.map{|q| q.to_s}.include?(b[:id])}.sort{|a, b| a[:level] <=> b[:level]}[ignore_index]
    end

    while building && !building[:available] do
      ignore_index += 1
      building = buildings.select{|b| building_ids.map{|q| q.to_s}.include?(b[:id])}.sort{|a, b| a[:level] <=> b[:level]}[ignore_index]
      return false if building.nil?
    end

    puts "Building is not available." and return if building.nil? || !building[:available]
    if building[:level] < building_level_cap
      if !is_building?
        puts "Building #{building[:name]} (+1) at #{planet} [#{humanize_seconds(building[:time])}]..."
        build(building[:id])
        queue_push(Time.now + building[:time], {planet: planet, name: :build_tactics, args: [planet, building_ids, building_level_cap]})
        return true
      else
        build_time = is_building?
        puts "Waiting for #{humanize_seconds(build_time)} at #{planet} to finish actual construction"
        queue_push(Time.now + build_time, {planet: planet, name: :build_tactics, args: [planet, building_ids, building_level_cap]})
        return true
      end
    else
      puts "false with #{building[:level]} < #{building_level_cap}" if DEBUG
      return false
    end
  end

  def research_tactics(planet, technology_ids, research_level_cap)
    switch_planet(planet, false)
    technology = technologies.select{|t| technology_ids.map{|q| q.to_s}.include?(t[:id])}.sort{|a, b| a[:level] <=> b[:level]}.first
    if technology.nil? || !technology[:available]
      puts "Technology is not available."
      return true
    end
    if technology[:level] < research_level_cap
      if !is_researching?
        research(technology)
        queue_push(Time.now + technology[:time], {planet: planet, name: :research_tactics, args: [planet, technology_ids, research_level_cap]})
      else
        research_time = is_researching?
        puts "Waiting for #{humanize_seconds(research_time)} at #{planet} to finish actual research"
        queue_push(Time.now + research_time, {planet: planet, name: :research_tactics, args: [planet, technology_ids, research_level_cap]})
        return true
      end
    else
      puts "false with #{filtered_structures} < #{building_level_cap}" if DEBUG
      return false
    end
    true
  end

  def colonize_tactics(from_planet, params, planet_size)
    puts "Begin trying to colonize planet at #{params}"
    if colonize_new_planet(from_planet, params, planet_size)
      queue_push(Time.now + 60*2, {planet: nil, name: :check_new_planets, args: [from_planet, planet_size, 1]})
    else
      queue_push(Time.now, {planet: nil, name: :check_new_planets, args: [from_planet, planet_size, 1]})
    end
  end

  def colonize_new_planet(from_planet, cords, planet_size)
    switch_planet(from_planet, false)
    build_fleet({'kolo': '1'})
    res = colonize(cords)
    if res
      return true
    else
      puts 'Colonize failed.'
    end
  end

  def farm_tactics(planet, galaxy_number, from_solar_system, to_solar_system)
    if from_solar_system <= to_solar_system
      puts "Scanning #{from_solar_system} solar system..."
      wait_time = attack_inactive_players(planet, galaxy_number, from_solar_system, from_solar_system)
      if wait_time == -1
        # case where we have actual 0 transporters to attack
        queue_push(Time.now + 60, {planet: planet, name: :farm_tactics, args: [planet, galaxy_number, from_solar_system, to_solar_system]})
      elsif wait_time == -2
        # case where we have reached max fleet limit
        queue_push(Time.now + 180, {planet: planet, name: :farm_tactics, args: [planet, galaxy_number, from_solar_system+1, to_solar_system]})
      else
        queue_push(Time.now, {planet: planet, name: :farm_tactics, args: [planet, galaxy_number, from_solar_system+1, to_solar_system]})
      end
    else
      puts "Finished plundering inactive players"
    end
  end

  def attack_inactive_players(planet, galaxy_number, from_solar_system, to_solar_system)
    switch_planet(planet, false)
    targets = find_inactive_planets(galaxy_number, from_solar_system, to_solar_system, true, false)
    if targets == -1
      return -1
    end
    times_array = [0]
    puts "Found #{targets.count} targets." if targets.count > 0
    targets.each_with_index do |target, i|
      resources_to_plunder = (target[:m] + target[:k] + target[:d])/2
      fleet = calculate_transporters(planet, resources_to_plunder)
      aggresive_fleet = calculate_agressive_fleet(planet, target)
      
      return -1 if !aggresive_fleet
      if fleet.values.inject(0){|sum,x| sum + x } > 0
        params = { 
          'speed': '10',
          'mission': '1',
          'resource1': '0',
          'resource2': '0',
          'resource3': '0',
          'planettype': '1',
          'galaxy': target[:cords][0],
          'system': target[:cords][1],
          'planet': target[:cords][2]
        }
        fleet.each{|k, v| params[k] = v}
        aggresive_fleet.each{|k, v| params[k] = v}
        puts "Sending fleet to #{target[:cords].join(':')} with #{fleet.values.inject(0){|sum,x| sum + x }} transporters and #{aggresive_fleet.values.inject(0){|sum,x| sum + x }} offensive fleet to plunder #{resources_to_plunder/1000000000} mld total resources"
        a=send_fleet(params)
        times_array << a
      else
        puts "Not enough fleet (transporters) to plunder #{resources_to_plunder/1000000000} mld resources at #{target[:cords].join(':')}"
        return -2
      end
    end
    return times_array.max
  end

  def calculate_agressive_fleet(planet, target)
    switch_planet(planet, false)

    enemy_fleet = Hash[*target[:fleet].split(', ')]
    enemy_fleet.delete("Satelita s\u0142oneczny")
    enemy_fleet_value = enemy_fleet.values.map{|q| q.to_s.gsub('.', '').to_i}
    enemy_fleet_value = enemy_fleet_value.inject(0){|sum,x| sum + x }
    if enemy_fleet_value > 0
      puts "enemy fleet: #{enemy_fleet}, skipping"
      return false
    end 

    defense = Hash[*target[:defense].split(', ')]
    defense.delete('Przeciwrakieta')
    defense.delete("Rakieta mi\u0119dzyplanetarna")
    defense_value = defense.values.map{|q| q.to_s.gsub('.', '').to_i}
    defense_value = defense_value.inject(0){|sum,x| sum + x }

    total_enemy_value = enemy_fleet_value*2 + defense_value
    current_fleet = available_fleet(planet)

    if current_fleet['ship207'].to_i >= total_enemy_value && current_fleet['ship204'].to_i >= total_enemy_value
      agressive_fleet_params = { 
        'ship207': total_enemy_value,
        'ship204': total_enemy_value,
      }
      return agressive_fleet_params
    else
      puts "Not enough firepower to plunder #{target[:cords].join(':')}"
      return false
    end
  end


  def find_inactive_planets(galaxy_number, from_solar_system, to_solar_system, inactive=true ,safe=true)
    delete_spy_inbox
    objectives = scan_galaxy(galaxy_number, from_solar_system, to_solar_system)
    
    # Select idle planets
    objectives.select!{|q| q[:player]['(i I)']} if inactive

    # Spy planets
    objectives.each_with_index do |p, i|
      next if p[:cords].empty? # happens to your own planets
      puts "Scanned #{i+1} planets so far..." if (i+1)%10 == 0
      r = auto_spy(p[:cords], FAST_SPY_PROBE_COUNT)
      if r[/Wszystkie sloty/]
        return -1 # sloty zajete
      end
      smart_random = rand + rand(2)
      smart_random = smart_random < 0.3543 ? 0.3543 + rand : smart_random
      sleep(smart_random)
    end

    # read raports
    targets = spy_inbox
    targets.select!{|q| q[:m].to_i + q[:k].to_i + q[:d].to_i >= RESOURCE_CAP_TO_PLUNDER}.sort!{|a, b| a[:m].to_i <=> b[:m].to_i} rescue []

    # save targets to file
    File.open('private_targets.rb', 'a+') do |file| 
      targets.each do |target|
        file.write("#{target},\n")
      end
    end

    targets.select!{|q| q[:fleet] == '' && q[:defense] == ''} if safe
    delete_spy_inbox
    return targets
  end

  def calculate_transporters(planet, resource_count)
    fleet = available_fleet(planet)
    fleet.select!{|k, v| ['ship217', 'ship203', 'ship202'].include?(k)}

    # define Hash
    fleet_available = {
      ship202: 0,
      ship203: 0,
      ship217: 0
    }

    fleet.keys.reverse.each do |key|
      while fleet[key].to_i > 0 do
        break if resource_count <= 0
        fleet[key] = fleet[key].to_i - 1
        fleet_available[key.to_sym] += 1
        resource_count -= FLEET_CAPACITY[key.to_sym]
      end
    end
    fleet_available  
  end

  def check_new_planets(from_planet, size, try)
    return if try <= 0
    new_planets = planets - KNOWN_PLANETS
    new_planets.each do |planet|
      planet_size = planet_size(planet)
      if planet_size < size
        switch_planet(planet, false)
        a, b, c = planet_position(planet)[0], planet_position(planet)[1], planet_position(planet)[2]
        puts "New planet #{planet_size} fields - destroying"
        destroy_planet(planet[3..-1], PASSWORD)
        queue_push(Time.now, {planet: nil, name: :colonize_tactics, args: [from_planet, colonize_params(a, b, c), size]})
      else
        # slack notifier?
        rename_planet(planet, 'new-x')
        puts "Succesfully colonized planet with #{planet_size(planet)} fields."
        queue_push(Time.now + 60*2, {planet: nil, name: :check_new_planets, args: [from_planet, size, try-1]})
      end
    end
    puts 'done checking new planets, next check in 2 minutes.'
    queue_push(Time.now + 60*2, {planet: nil, name: :check_new_planets, args: [from_planet, size, try]})

  end

  def rename_planet(planet, name)
    switch_planet(planet, false)
    params = {'newname': name.to_s,
      'action': 'Zmień nazwę'}
    res = post(@universe_address + '/index.php?page=overview_rename&pl=' + planet[3..-1], params)
  end

  def queue_push(time, operation)
    @queue = [] if !@queue
    @queue << {time: time, operation: operation}
    puts "pushing queue: #{{time: time, operation: operation}} (after #{time - Time.now}) seconds" if DEBUG
    @queue.sort!{|a, b| b[:time] <=> a[:time]}
  end

  def execute(operation)
    if operation.class.name == 'Hash' && operation[:name] && operation[:args]
      puts "eval: #{operation[:name]} with #{operation[:args]}" if DEBUG
      send(operation[:name], *operation[:args])
    end
  end

  def print_queue
    queue.reverse.each do |q|
      puts "time: #{q[:time] - Time.now}, operation: #{q[:operation]}"
    end
  end

  def colonize_params(ax, bx, cx)
    {
      'galaxy': ax.to_s,
      'system': bx.to_s,
      'planet': cx.to_s # największe są ponoć na 6-7 
    }
  end

  def main_loop
    puts "Queued #{queue.count} jobs."
    while queue.count > 0 do
      if queue.last[:time] < Time.now
        execute(queue.pop[:operation])
        puts "Queued #{queue.count} jobs."
      else
        puts "Waiting another #{humanize_seconds((queue.last[:time] - Time.now).round)}"
        sleep((queue.last[:time] - Time.now)/2 < 32 ? ((queue.last[:time] - Time.now)/2 + 2) : (queue.last[:time] - Time.now)/2) 
      end
    end
    puts "Finished queued jobs!"
  end
end
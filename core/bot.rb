class Bot < SessionHandler

  def initialize(username=nil, password=nil, universe_address=nil)
    @queue = []
    if username && password && universe_address
      @username = username
      @password = password
      @universe_address = universe_address
    else
      puts 'You need to pass username, password and universe address as args' and return
    end

    File.new('private_targetsss.rb', 'w') if !File.exist?('private_targets.rb')
    
    login
    if @sin && @headers
      true
    else
      false
    end
  end

  def switch_planet(planet_address, show_message=true)
    res = get(@universe_address + '/index.php?page=overview&mode=&re=0' + '&' + planet_address)
    puts "Current planet: #{planet_address}" if show_message
    sleep(1)
    true
  end

  def research(tech)
    puts "Researching #{tech[:name]} (#{tech[:level]}+1) [#{humanize_seconds(tech[:time])}]"
    res = get(@universe_address + '/index.php?page=buildings&mode=research&cmd=search&tech=' + tech[:id].to_s)
    if ['302', '200'].include?(res.code)
      return res
    end
    false
  end

  def is_researching?
    res = get(@universe_address + '/index.php?page=buildings&mode=research')
    if res.body['Anuluj']
      if res.body[/ss\s=\s\d{1,5}/]
        return res.body[/ss\s=\s\d{1,5}/][/\d{1,5}/].to_i
      end
    end
    false
  end

  def technologies
    res = get(@universe_address + '/index.php?page=buildings&mode=research')
    re = /<a href="index.php\?page=infos&gid=[^>]*>[^<>]*<\/a>/m
    techs = []
    res.body.scan(re) do |match|
      techs << match.to_s
    end
    techs_parsed = []
    techs.each do |raw_match|
      level_parsed = res.body[res.body.index(raw_match)+raw_match.size..res.body.index(raw_match)+raw_match.size+25][/Poziom\s\d{1,2}/].split(' ').last.to_i rescue 0 
      available = res.body[res.body.index(raw_match)..-1][res.body[res.body.index(raw_match)..-1].index('Wymagane surowce')..res.body[res.body.index(raw_match)..-1].index('Czas budowy')][/red/] == nil ? true : false
      resources = res.body[res.body.index(raw_match)..-1][res.body[res.body.index(raw_match)..-1].index('Wymagane surowce')..res.body[res.body.index(raw_match)..-1].index('Czas budowy')].gsub(/<[^>]*>/, '').gsub(/\\.|\s/, '')[16..-2]
      techs_parsed << { 
        id: raw_match[/gid=\d{3}/][/\d{3}/],
        name: raw_match.gsub(/<[^>]*>/,'').strip,
        level: level_parsed,
        time: ogame_time_to_seconds(res.body[res.body.index(raw_match)..-1][/\d{2}h\s\d{2}m\s\d{2}s/]),
        available: available,
        resources: {
          m: resources[/Metal:(\d{1,3}\.?)+/].to_s[/(\d{1,3}\.?)+/].to_s.gsub('.', ''), 
          k: resources[/Kryszta[^:]*:(\d{1,3}\.?)+/].to_s[/(\d{1,3}\.?)+/].to_s.gsub('.', ''), 
          d: resources[/Deuter:(\d{1,3}\.?)+/].to_s[/(\d{1,3}\.?)+/].to_s.gsub('.', '')
        }
      }
    end
    techs_parsed
  end

  def buildings
    res = get(@universe_address + '/index.php?page=buildings')
    re = /<a href="index.php\?page=infos&gid=[^>]*>[^<>]*<\/a>/m
    buildings = []
    res.body.scan(re) do |match|
        buildings << match.to_s
    end
    buildings_parsed = []
    buildings.each do |raw_match| 
      available = res.body[res.body.index(raw_match)..-1][res.body[res.body.index(raw_match)..-1].index('Wymagane surowce')..res.body[res.body.index(raw_match)..-1].index('Czas budowy')][/red/] == nil ? true : false
      resources = res.body[res.body.index(raw_match)..-1][res.body[res.body.index(raw_match)..-1].index('Wymagane surowce')..res.body[res.body.index(raw_match)..-1].index('Czas budowy')].gsub(/<[^>]*>/, '').gsub(/\\.|\s/, '')[16..-2]
      buildings_parsed << {
        id: raw_match[/gid=\d{1,2}/][/\d{1,2}/], 
        name: raw_match.gsub(/<[^>]*>/,'').strip,
        level: raw_match.gsub(/<[^>]*>/,'').strip[/\d{1,2}/].to_i,
        time: ogame_time_to_seconds(res.body[res.body.index(raw_match)..-1][/\d{2}h\s\d{2}m\s\d{2}s\s/].gsub("\n", '')),
        available: available,
        resources: {
          m: resources[/Metal:(\d{1,3}\.?)+/].to_s[/(\d{1,3}\.?)+/].to_s.gsub('.', ''), 
          k: resources[/Kryszta[^:]*:(\d{1,3}\.?)+/].to_s[/(\d{1,3}\.?)+/].to_s.gsub('.', ''), 
          d: resources[/Deuter:(\d{1,3}\.?)+/].to_s[/(\d{1,3}\.?)+/].to_s.gsub('.', '')
        }
      }
    end
    buildings_parsed
  end

  def build(structure_id)
    res = get(@universe_address + '/index.php?page=buildings&action=insert&build=' + structure_id.to_s)
    puts "Building id: #{structure_id}." if DEBUG
    res
  end

  def is_building?
    res = get(@universe_address + '/index.php?page=buildings')
    i = res.body.index('Koniec')
    ogame_time_to_seconds(res.body[i..i+1000][/\d{1,2}h\s\d{1,2}m\s\d{1,2}s/]) rescue false
  end

  def build_fleet(params)
    sleep(1)
    data = {
      'mode': 'addit',
      'sin': @sin[4..-1],
      'malyt': '0',
      'duzyt': '0',
      'lm': '0',
      'cm': '0',
      'kraz': '0',
      'ow': '0',
      'kolo': '0',
      'rec': '0',
      'sonda': '0',
      'sat': '0',
      'nisz': '0',
      'gs': '0',
      'ft': '0',
      'ut': '0'
    }
    params.each{|k, v| data[k] = v}
    res = post(@universe_address + '/nowastocznia.php', data)
    if ['302', '200'].include?(res.code)
      message = res.body[res.body.index(/Komunikat gry/)..-1][/<th>.*/]
      .gsub(/(<[^>]*>)/, '')
      .gsub("\r", '')
      .gsub("\n", '').strip
    end

    puts message if DEBUG
  end
  
  def resources(planet)
    switch_planet(planet)
    res = get(@universe_address + '/index.php?page=resources')
    html_page = res.body

    metal = html_page[html_page.index('Kopalnia metalu')..html_page.index('Kopalnia metalu')+100]
    metal = metal[metal.index('<font')..metal.index('</font')-1].gsub(/<[^>]*>/, '').gsub('.', '')

    krysztal = html_page[html_page.index('Kopalnia kryszta')..html_page.index('Kopalnia kryszta')+100]
    krysztal = krysztal[krysztal.index('<font')..krysztal.index('</font')-1].gsub(/<[^>]*>/, '').gsub('.', '')

    deuter = html_page[html_page.index('Ekstraktor deuteru')..html_page.index('Ekstraktor deuteru')+100]
    deuter = deuter[deuter.index('<font')..deuter.index('</font')-1].gsub(/<[^>]*>/, '').gsub('.', '')

    {
      metal: metal,
      krysztal: krysztal,
      deuter: deuter
    }
  end

  def send_fleet(fleet_params={})
    res = get(@universe_address + '/index.php?page=fleet')

    form_data = parse_hidden_input_fields(res.body)
    
    # form_data['ship203'] = form_data['maxship203'] # set ship count to max avilable
    FLEET_PARAMS.keys.each do |ship_key|
      form_data[ship_key] = fleet_params[ship_key.to_sym] if !fleet_params[ship_key.to_sym].nil?
      form_data[ship_key] = fleet_params[ship_key] if !fleet_params[ship_key].nil?
    end

    form_data['ship203'] = form_data['maxship203'] if fleet_params[:ship203] == 'max'
    form_data['ship202'] = form_data['maxship202'] if fleet_params[:ship202] == 'max'
    form_data['ship217'] = form_data['maxship217'] if fleet_params[:ship217] == 'max'
    #fleet_params.each{|k, v| data[k] = v}
    res = post(@universe_address + '/floten1.php?page=fleet', form_data)
    sleep(1)
    puts res.body if DEBUG
    if ['302', '200'].include?(res.code)
      form_data = parse_hidden_input_fields(res.body)

      form_data['galaxy'] = fleet_params[:galaxy]
      form_data['system'] = fleet_params[:system]
      form_data['planet'] = fleet_params[:planet]
      form_data['speed'] = '10'
      res = post(@universe_address + '/floten2.php?page=fleet', form_data)
      sleep(1)
      puts res.body if DEBUG
      if ['302', '200'].include?(res.code)
        form_data = parse_hidden_input_fields(res.body)

        form_data['mission'] = fleet_params[:mission]
        form_data['resource1'] = fleet_params[:resource1]
        form_data['resource2'] = fleet_params[:resource2]
        form_data['resource3'] = fleet_params[:resource3]
        form_data['planettype'] = '1'

        # all:
        # form_data['resource1'] = form_data['thisresource1']
        # form_data['resource2'] = form_data['thisresource2']
        # form_data['resource3'] = form_data['thisresource3']

        res = post(@universe_address + '/floten3.php?page=fleet', form_data)
        sleep(1)
        puts res.body if DEBUG
        if ['302', '200'].include?(res.code)
          puts 'Fleet probably sent, kappa.' if DEBUG
          if res.body[/Blokada skryptu./]
            return 'Blokada skryptu.'
          end
          time = res.body[res.body.index("Godzina dotarcia floty")..res.body.index("Godzina dotarcia floty")+50].gsub(/<[^>]*>/, '')[/([a-zA-z]{3}\s){2}\d{2}\s\d{2}:\d{2}:\d{2}/] rescue 0
          return (Time.parse(time) - Time.now) rescue 0
        else
          return res
        end
      end
    end
  end

  def colonize(params)
    galaxy(params[:galaxy], params[:system])
    res = get(@universe_address + '/ajaxsend.php?mission=7&galaxy=' + params[:galaxy] + '&system=' + params[:system] + '&planet=' + params[:planet] + '&type=1&ships=1&kod=undefined?', false)
    if res.body[/Flota zosta/]
      true
    else
      false
    end
  end

  #private

  def ogame_time_to_seconds(str)
    seconds = str[/\d{2}s/][0..-2].to_i
    minutes = str[/\d{2}m/][0..-2].to_i
    hours   = str[/\d{2}h/][0..-2].to_i
    return seconds + minutes*60 + hours*60*60
  end

  def parse_hidden_input_fields(html_page)
    re = /<input type="hidden"[^>]*>/m
    hidden_input_fields = []
    html_page.scan(re) do |match|
      hidden_input_fields << match.to_s
    end

    form_data = {}
    hidden_input_fields.each do |input|
      form_data[input[/name="[^"]*"/][6..-2]] = input[/value="[^"]*"/][7..-2]
    end

    form_data['sin'] = @sin[4..-1]
    return form_data
  end

  def humanize_seconds(t)
    if t > 60*60
      Time.at(t).utc.strftime("%Hh %Mm %Ss")
    else
      Time.at(t).utc.strftime("%Mm %Ss")
    end
  end

  def planets
    r = get(@universe_address + '/index.php?page=overview')
    raw_planets = r.body[r.body.index('<select')..r.body.index('</select')]
    planets_ = []
    
    re = /cp=\d+/m
    raw_planets.scan(re) do |match|
      planets_ << match.to_s
    end
    planets_
  end

  def destroy_planet(planet_id, password)
    puts "!!! PLANET #{planet_id} DESTROYED!" 
    params = {'pw': password,
    'action': 'Skasuj!',
    'kolonieloeschen': '1',
    'deleteid': planet_id.to_s }
    res = post(@universe_address + '/index.php?page=overview_rename&pl=' + planet_id.to_s, params)
  end

  def planet_size(planet)
    switch_planet(planet)
    res = get(@universe_address + '/index.php?page=overview')
    return res.body[res.body.index('Maksymalna ilo')..res.body.index('Maksymalna ilo')+35][/\d{3}/].to_i rescue 999
  end

  def galaxy(galaxy, solar_system)
    res = get(@universe_address + '/ajaxgalaxy.php?galaxy=' + galaxy.to_s + '&system=' + solar_system.to_s + '?')
    doc = Nokogiri::HTML(res.body)
    table = doc.at('table')
    galaxy = []
    table.search('tr')[1..-3].each do |tr|
      cells = tr.search('td')

      position = cells[0].text
      planet_name = cells[2].text
      player = cells[5].text
      fast_spy_raw = cells[7].search('a')[0].attributes['onclick'].value[/\([^\)]*\)/][1..-2].split(', ') rescue ''
      fast_spy_args = [fast_spy_raw[1], fast_spy_raw[2], fast_spy_raw[3], fast_spy_raw[6][1..-2]] rescue ''
      galaxy << {planet_name: planet_name, player: player, cords: fast_spy_args} if !player.empty?
    end
    galaxy
  end

  def auto_spy(cords_arr, count)
    # cords_arr = [galaxy, solar_system, planet_position, code]
    # custom ajax request with unusual headers
    url = @universe_address + '/ajaxsend.php?mission=6&galaxy=' + cords_arr[0].to_s + '&system=' + cords_arr[1].to_s + '&planet=' + cords_arr[2].to_s + '&type=1&ships=' + count.to_s + '&kod=' + cords_arr[3] + '?'
    uri = URI(url)
    query = Net::HTTP::Get.new(uri)

    if @headers
      @headers.each do |k, v|
        query.add_field(k.to_s, v)
      end
    end
    query.add_field('Referer', @universe_address + '/index.php?page=galaxy&' + @sin)
    query.add_field('Accept', '*/*')
    query.add_field('If-Modified-Since', 'Sat, 1 Jan 2006 00:00:00 GMT')
    response = Net::HTTP.start(uri.hostname, uri.port) {|http|
      http.request(query)
    }
    return response.body.gsub(/<[^>]*>/, '')

  end

  def spy_inbox
    res = get(@universe_address + '/index.php?page=messages&mode=show&messcat=0')
    doc = Nokogiri::HTML(res.body)
    tables = doc.search('table')
    messages = []
    msg = {} 
    tables[6].search('table').each do |t|
      last_valid = false
      if t.search('td').count == 11
        last_valid = true
        msg[:cords] = t.search('td')[0].text[/\[[0-9\:]*\]/][1..-2].split(':') rescue nil
        msg[:m] = t.search('td')[2].text.gsub('.', '').to_i
        msg[:k] = t.search('td')[5].text.gsub('.', '').to_i
        msg[:d] = t.search('td')[7].text.gsub('.', '').to_i
        msg[:value] = (msg[:m] + msg[:k] + msg[:d]) / 1000000000
      elsif t.search('td')[0].text == 'Flota'
        msg[:fleet] = t.search('td').map{|q| q.text}.delete_if{|q| ['\u00A0', 'Flota', '', ' ', '  ', "\u00A0"].include? q}.join(', ')
      elsif t.search('td')[0].text == 'Obrona'
        msg[:defense] = t.search('td').map{|q| q.text}.delete_if{|q| ['\u00A0', 'Obrona', '', ' ', '  ', "\u00A0"].include? q}.join(', ')
      elsif t.search('td')[0].text == 'Konstrukcja'
        msg[:konstrukcja] = nil#t.search('td').map{|q| q.text}.join(', ')
      elsif t.search('td')[0].text == 'Badanie'
        msg[:badanie] = nil#t.search('td').map{|q| q.text}.join(', ')
      elsif t.search('td')[0].text[/resort/]
        messages << msg if msg && !messages.include?(msg)
        msg = {}
      end
    end
    messages
  end

  def delete_spy_inbox
    params = {'messages': '1',
      'deletemessages2': 'deleteall',
      'deletemessages': 'deleteall',
      'category': '0' }
    res = post(@universe_address + '/index.php?page=messages', params)
  end

  def scan_galaxy(galaxy_number, from_solar_system, to_solar_system)

    inhabited_planets = []
    (from_solar_system..to_solar_system).each do |i|
      res = galaxy(galaxy_number, i)
      inhabited_planets = inhabited_planets + res
    end
    
    inhabited_planets
  end

  def available_fleet(planet)
    switch_planet(planet, false)
    res = get(@universe_address + '/index.php?page=fleet')
    form_data = parse_hidden_input_fields(res.body)
    fleet = {}
    form_data.keys.select{|q| q['max']}.map{|k| fleet[k[3..-1]] = form_data[k]}
    fleet
  end

  def planet_position(planet)
    switch_planet(planet, false)
    res = get(@universe_address + '/index.php?page=overview')
    res.body[res.body.index('Pozycja:')..res.body.index('Pozycja:')+150][/\[\d{1,2}:\d{1,3}:\d{1,2}\]/][1..-2].split(':')
  end

end
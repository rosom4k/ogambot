require 'net/http' 
require 'json'
require 'time'
require 'nokogiri' # parsing html

require_relative 'private_constants.rb'

require_relative 'core/constants.rb'
require_relative 'core/session_handler.rb'
require_relative 'core/bot.rb'
require_relative 'core/tactics.rb'

### LOGOWANIE ###
b = Bot.new(USERNAME, PASSWORD, UNIVERSE_ADDRESS)

KNOWN_PLANETS = b.planets if !defined? KNOWN_PLANETS

# Tactic to farm inactive players by attacking them
# with transporters and OW + LM. Only rule is 0 fleet
# during scan and resources greater than constant       
# value.

# cords = {galaxy: '10',
# system: '272',
# planet: '6'
# }
# b.colonize_tactics(planet, cords, 400)
# b.main_loop

# Research tactic - find lowest level technology
# and research it. Recursive unless you have 
# resources for at least one technology.
# technology_ids = [108]
# b.research_tactics(planet, technology_ids, 20)

# # Tactic used to build planet by finding
# # lowest level structure and building it.
planet = 'cp=100041'
# b.build_tactics(new_planet, [14], 20)

# new_planet = 'cp=100043'
# b.build_tactics(new_planet, [14], 20)

# new_planet = 'cp=99296'
# b.build_tactics(new_planet, [14], 20)

### Main queue-based loop

b.farm_tactics(planet, 4, 1, 499)
b.main_loop

b.farm_tactics(planet, 3, 1, 499)
b.main_loop

b.farm_tactics(planet, 5, 1, 499)
b.main_loop

b.farm_tactics(planet, 2, 1, 499)
b.main_loop

# t = eval('[' + File.open('private_targets.rb').read + ']')
# t.sort!{|a, b| a[:value] <=> b[:value]}
# t.reverse.each do |target|
#   b.farm_tactics(planet, target[:cords][0].to_i, target[:cords][1].to_i, target[:cords][1].to_i)
# end
# b.main_loop
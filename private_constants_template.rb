USERNAME = '<your username>'
PASSWORD = '<your password>'
UNIVERSE_ADDRESS = 'http://uni9.ogam.net.pl' 

IGNORED_TECHS_RAW = [199] # technologia grawitonow
IGNORED_TECHS = IGNORED_TECHS_RAW + IGNORED_TECHS_RAW.map{|q| q.to_s}
IGNORED_BUILDINGS_RAW = [4, 44] # elektrownia, silos rakietowy
IGNORED_BUILDINGS = IGNORED_BUILDINGS_RAW + IGNORED_BUILDINGS_RAW.map{|q| q.to_s}
FAST_SPY_PROBE_COUNT = 5000

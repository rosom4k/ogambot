

## Setup

1. Install rvm in your system `https://rvm.io/` with bundler and ruby >= 2.3
2. Clone repository to your computer 
3. Execute `Bundle install` in main directory

---
## Usage

_Probably you want to execute `main.rb` file as it contain some tactics, but first_

1. Edit `constants.rb` and paste your own constants here (planet addresses, etc.)
2. Copy `private_constants_template.rb` to `private_constants.rb` and replace it content
3. Execute `ruby main.rb` in terminal
---